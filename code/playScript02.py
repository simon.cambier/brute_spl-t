"""
playScript02.py

Usage:
------
python playScript02.py -boardwidth -boardHeight

Description:
------------
Simulates games of SPL-T starting from a blank board, with randomly chosen splits.
Runs parallel instances on all CPU cores. Builds up a 2D histogram of the branching factor

Hard coded parameters:
	gamesPerBatch	How many games to play before saving to file
	numBatches		How many batches to run

	(Adjust these to make disk writes more or less frequent)

	maxNumChoices
	maxNumMoves
		How large to make the output 2D histogram - the script will crash if it is too small.
		For an 8x16 board, (choices,moves)=(50,400) is OK


Output:
-------
Generates 2 output files per instance:

[playScript02#]rXc.txt
		Length and score for all games simulated

[playScript02#]rXc_choiceImage.dat"
		2D histogram of how many times N choices were available on the Mth move.

(#=instance label (a,b,c,d,e,f,g...), rXc = boardWidth x boardHeight):

If these output files already exist, they are appended to rather than overwritten.

"""
import sys 
import os
import core
import multiprocessing
import random

from pylab import zeros

gamesPerBatch=200
numBatches=100000


# Image size
maxNumChoices=50
maxNumMoves=400

##########################################
def playSplitRandomly(gameBoard):
	#Play a single game of SPL-T
##########################################	
	numChoices=[]
	while 1:

		moveOptions=gameBoard.getMoveOptions()


		if len(moveOptions)==0:
			return gameBoard.score,gameBoard.splitRecord,numChoices
			break

		nextMove=random.choice(moveOptions)

		numChoices.append(len(moveOptions))

		core.makeMove(gameBoard,nextMove)


##########################################
def PlayGames(boardWidth,boardHeight,saveLabel,):
	# Play many games of SPL-T
##########################################

	saveFile_1_Name = "[playScript02"+saveLabel+"]{0}x{1}.txt".format(boardWidth,boardHeight)
	
	if not os.path.isfile(saveFile_1_Name):
		saveFile=open(saveFile_1_Name,'w')
		saveFile.write("Score\tLength\n")
		saveFile.close()
	
	#For each game played, keep a record of:
	paths=[]
	pathLengths=[]	
	scores=[]
	


	for batch in range(numBatches):
		numChoicesList=[]
		for games in range(gamesPerBatch):

			# Initialize the board
			gameBoard=core.Board(boardWidth,boardHeight)

			# Play
			score,path,numChoices=playSplitRandomly(gameBoard)

			# Save the results to buffer
			numChoicesList.append(numChoices)
			paths.append(path)
			pathLengths.append(len(path))
			scores.append(score)
			print("playScript02"+saveLabel+","+"\t #: ",games,"\tBatch: ",batch,"\tLength: ",len(path))

		
		# After each batch, save the buffers to file them clear them
		saveFile=open(saveFile_1_Name,'a')
		for index,path in enumerate(paths):
			saveFile.write("{0}\t{1}\n".format(scores[index],pathLengths[index]))
		saveFile.close()

		# Read in the latest version of the saved choiceImage
		choiceImageFileName="[playScript02{0}]{1}X{2}_choiceImage.dat".format(saveLabel,boardWidth,boardHeight)
		choiceImage=zeros((maxNumChoices,maxNumMoves))
		saveFile=open(choiceImageFileName,'r')
	
		column=0
		for line in saveFile:
			data=line.rstrip('\n').split("\t")
			for index in range(maxNumChoices):
				choiceImage[index,column]=int(data[index])
			column+=1	
		saveFile.close()		

		# Add your new data
		for numChoices in numChoicesList:
			for index,element in enumerate(numChoices):
					choiceImage[element,index]+=1	


		# Save it back to file
		saveFile=open(choiceImageFileName,'w')
		for column in range(maxNumMoves):
			for row in range(maxNumChoices):
				saveFile.write("{0}\t".format(int(choiceImage[row][column])))
			saveFile.write("\n")
		saveFile.close()		


		paths=[]
		scores=[]	
		pathLengths=[]


##########################################
if __name__ == '__main__':
##########################################

	try:
		boardWidth=int(sys.argv[1])
		boardHeight=int(sys.argv[2])
	except:
		print("Didn't get the parameters I expected.\n\nplayScript02.py <boardwidth> <boardHeight>\n")
		sys.exit(1)

	print("\n--------Random SPL-T--------\n")

	if boardWidth not in [4,8,16,32,64,128,256,512,1024]:
		print("Invalid board width ({0}). Should be one of (4,8,16,32,64,128,256,512,1024)".format(boardWidth))
		sys.exit(1)
	if boardHeight not in [4,8,16,32,64,128,256,512,1024]:
		print("Invalid board height ({0}). Should be one of (4,8,16,32,64,128,256,512,1024)".format(boardHeight))
		sys.exit(1)	

	numCores=multiprocessing.cpu_count()
	print("\nCPU cores available=",numCores)    

	saveLabelList=['a','b','c','d','e','f','g','h','i','j','k','l']
	
	choiceImage=zeros((maxNumChoices,maxNumMoves))

	choiceImageFileNameList=[]
	for coreIndex in range(numCores):
		choiceImageFileNameList.append("[playScript02{0}]{1}X{2}_choiceImage.dat".format(saveLabelList[coreIndex],boardWidth,boardHeight))
	
	for choiceImageFileName in choiceImageFileNameList:
		if os.path.isfile(choiceImageFileName):
			print("Found an exisiting histogram, will append to it")

		else:
			saveFile=open(choiceImageFileName,'w')
			for column in range(maxNumMoves):
				for row in range(maxNumChoices):
						saveFile.write("{0}\t".format(int(choiceImage[row][column])))

				saveFile.write("\n")
			saveFile.close()	



	### Run the function PlayGames on all available CPU cores in parallel
	jobs=[]
	for index in range(numCores):
		process = multiprocessing.Process(target=PlayGames, args=(boardWidth,boardHeight,saveLabelList[index],))
		jobs.append(process)
		process.start()

	for job in jobs:
		job.join()	

	sys.exit()
