"""
playScript03.py

Usage:
------
python playScript03.py -boardwidth -boardHeight -maxDepth

Description:
------------
Measure the size of SPL-T game tree by recursively playing all possible games up to 
a specified length. 

Output:
-------
	prints to console only
	Gives how many leaves/nodes were found at each depth, as well as how many
	game-ending choices were found.

"""

import sys 
import os
import core
import copy
import random



##########################################
def getBoardFromSequence(boardWidth,boardHeight,sequence):
		# Recreate a game board by replaying the sequence that leads to it
##########################################
	gameBoard=core.Board(8,16)
	tempSequence=sequence[:]

	while 1:
		if len(tempSequence)==0:
			return gameBoard	

		chosenBox=tempSequence.pop(0)
		core.makeMove(gameBoard,chosenBox)	


##########################################
def playSplitExhaustively(gameBoard,maxDepth,leafCount,deadLeafCount):
	# Recursively play all possible games up to a specified depth
##########################################	

	moveOptions=gameBoard.getMoveOptions()

	leafCount[len(gameBoard.splitRecord)]+=len(moveOptions)

	if len(gameBoard.splitRecord)==maxDepth:
		return
	if len(moveOptions)==0:
		deadLeafCount[len(gameBoard.splitRecord)-1]+=1
		return

	for nextMove in moveOptions:
		nextGameBoard=copy.deepcopy(gameBoard)
		core.makeMove(nextGameBoard,nextMove)
		playSplitExhaustively(nextGameBoard,maxDepth,leafCount,deadLeafCount)



##########################################
if __name__ == '__main__':
##########################################

	try:
		boardWidth=int(sys.argv[1])
		boardHeight=int(sys.argv[2])
		maxDepth=int(sys.argv[3])
	except:
		print("\nProblem interpreting arguments\n\nExpected usage is playScript03.py <boardWidth> <boardHeight> <maxDepth>")	
		sys.exit(1)

	if boardWidth not in [4,8,16,32,64,128,256,512,1024]:
		print("Invalid board width ({0}). Should be one of (4,8,16,32,64,128,256,512,1024)".format(boardWidth))
		sys.exit(1)
	if boardHeight not in [4,8,16,32,64,128,256,512,1024]:
		print("Invalid board height ({0}). Should be one of (4,8,16,32,64,128,256,512,1024)".format(boardHeight))
		sys.exit(1)	
		
	leafCount=[]
	deadLeafCount=[]

	for ii in range(maxDepth+1):
		leafCount.append(0)
		deadLeafCount.append(0)

	print("\n--------Recursive SPL-T--------\n")
	StartingBoard=getBoardFromSequence(boardWidth,boardHeight,[])
	playSplitExhaustively(StartingBoard,maxDepth,leafCount,deadLeafCount)

	print("leafCount:\t",leafCount)
	print("deadLeafCount:\t",deadLeafCount)

